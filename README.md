# 使い方
1. .envに以下の情報を記述します。LINE PAYのキーは公式のSandboxで取得します。LINE_PAY_CONFIRM_URLは、httpsから始まるサーバーのURLを入力します。
```
LINE_PAY_CHANNEL_ID= SandboxのチャンネルID
LINE_PAY_CHANNEL_SECRET= Sandboxのチャンネルシークレット
LINE_PAY_CONFIRM_URL=https://
LINE_BOT_CHANNEL_SECRET = LINE Developersで取得できるチャンネルシークレット
LINE_BOT_ACCESS_TOKEN = LINE Developersで取得できるアクセストークン
```

2. nodeコマンドでサーバーを起動。
```
node index.js
```

3. ngrokなどで、httpsのURLを発行し、LINE Developersで Webhook URLに設定。