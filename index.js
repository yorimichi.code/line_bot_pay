"use strict"

// Import environment variables from .env file.
require("dotenv").config();

// Import packages.
const express = require("express");
const app = express();
const uuid = require("uuid/v4");
const cache = require("memory-cache");

// Instanticate LINE Pay API SDK.
const line_pay = require("line-pay");
const pay = new line_pay({
    channelId: process.env.LINE_PAY_CHANNEL_ID,
    channelSecret: process.env.LINE_PAY_CHANNEL_SECRET,
    isSandbox: true
})

// Instantiate LINE Messaging API SDK.
const lineBot = require("@line/bot-sdk");
const botConfig = {
    channelAccessToken: process.env.LINE_BOT_ACCESS_TOKEN,
    channelSecret: process.env.LINE_BOT_CHANNEL_SECRET
}
const botMiddleware = lineBot.middleware(botConfig);
const botClient = new lineBot.Client(botConfig);

// Middleware configuration to serve static file.
app.use(express.static(__dirname + "/public"));

// Set ejs as template engine.
app.set("view engine", "ejs");

// Launch server.
app.listen(process.env.PORT || 3333, () => {
    console.log(`server is listening to ${process.env.PORT || 3333}...`);
});

// Webhook for Messaging API.
app.post("/webhook", botMiddleware, (req, res, next) => {
    res.sendStatus(200);

    req.body.events.map((event) => {
        // We skip connection validation message.
        if (event.replyToken == "00000000000000000000000000000000" || event.replyToken == "ffffffffffffffffffffffffffffffff") return;

        // We process payment if user says "コーヒー".
        if (event.type === "message"){
            if (event.message.text === "コーヒー") {
                const productName = "コーヒー";
                const price = 120;
                const quantity = 1;

                let packages = [
                    {
                        "id" : 1,
                        "amount": price * quantity,
                        "products" : [
                            {
                                "id" : "001",
                                "name" : productName,
                                "imageUrl" : "https://scdn.line-apps.com/n/channel_devcenter/img/fx/01_1_cafe.png",
                                "quantity" : quantity,
                                "price" : price
                            },
                        ]
                    }
                ]

                let options = {
                    amount: price * quantity,
                    currency: "JPY",
                    orderId: uuid(),
                    packages,
                    redirectUrls : {
                        confirmUrl: `${process.env.LINE_PAY_CONFIRM_URL}/pay/confirm`,
                        cancelUrl : `${process.env.LINE_PAY_CONFIRM_URL}/pay/cancel`,
                    },
                }

                pay.reserve(options).then((response) => {
                    let reservation = options;
                    reservation.transactionId = response.info.transactionId;
                    reservation.userId = event.source.userId;

                    console.log(`Reservation was made. Detail is following.`);
                    console.log(reservation);

                    // Save order information
                    cache.put(reservation.transactionId, reservation);
            
                    let message = {
                        type: "template",
                        altText: `${productName}を購入するには下記のボタンで決済に進んでください`,
                        template: {
                            type: "buttons",
                            text: `${productName}を購入するには下記のボタンで決済に進んでください`,
                            actions: [
                                {type: "uri", label: "LINE Payで決済", uri: response.info.paymentUrl.web},
                            ]
                        }
                    }
                    return botClient.replyMessage(event.replyToken, message);
                })
            }
        }
    });
});

// If user approve the payment, LINE Pay app call this webhook.
app.get("/pay/confirm", (req, res, next) => {
    if (!req.query.transactionId){
        console.log("Transaction Id not found.");
        return res.status(400).send("Transaction Id not found.");
    }

    // Retrieve the reservation from database.
    let reservation = cache.get(req.query.transactionId);
    if (!reservation){
        console.log("Reservation not found.");
        return res.status(400).send("Reservation not found.")
    }

    console.log(`Retrieved following reservation.`);
    console.log(reservation);

    let confirmation = {
        transactionId: req.query.transactionId,
        amount: reservation.amount,
        currency: reservation.currency
    }

    console.log(`Going to confirm payment with following options.`);
    console.log(confirmation);

    // Capture payment.
    pay.confirm(confirmation).then((response) => {

        // Reply to user that payment has been completed.
        let messages = [
            {
                type: "sticker",
                packageId: 2,
                stickerId: 144
            },
            {
                type: "text",
                text: `ありがとうございます。\n${reservation.packages[0].products[0].name}の決済が完了しました。`
            }
        ]

        botClient.pushMessage(reservation.userId, messages);

        return res.render(__dirname + "/confirm");
    });
});

app.get("/pay/cancel", (req, res, next) => {
    return res.render(__dirname + "/cancel");
})